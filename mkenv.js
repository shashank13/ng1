#!/usr/local/bin/node

'use strict';
const fs = require('fs');
const readline = require('readline');
const readlineSync = require('readline-sync');
const chalk = require('chalk');
const path = require('path');

/**
 * function to generate .env file from a template
 */
let mkenv = (file) => {
  // Return if .env already exist.
  if ( fs.existsSync('.env') ) return;

  let templateFile = (file) ? file : '.env.template';
  let lineReader = require('readline').createInterface({
    input: require('fs').createReadStream(templateFile)
  });
  
  let envConfig = {};
  
  // Read .env.template file.
  console.log(chalk.yellow.bold(`\n-> Reading .env.template file... \n`));
  lineReader.on('line', (line) => {
    let lineSplit = line.toString('utf-8').split('='),
        key = lineSplit[0],
        value = lineSplit[1];
    envConfig[key] = value;
  });
  
  // Write .env file
  lineReader.on('close', () => {
    let writeStream = fs.createWriteStream('.env'); // Create write stream.
    console.log(chalk.blue(`Set value for each project ENV variable;`));
    Object.keys(envConfig).forEach(config => {
      let value = readlineSync.question(chalk.blue(`\nset value for ${chalk.green(config)}\n> `), {
        hideEchoBack: false
      });
      writeStream.write(`${config.trim()}=${value}\n`)
    });
    writeStream.end(); // End write stream.
    console.log(chalk.green(`\n* '.env' created in the path => ${path.resolve('')}.`));
    console.log(chalk.green(`* '.env' can be manually updated.\n`));
  });
}

module.exports = mkenv();