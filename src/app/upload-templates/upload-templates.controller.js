export default class UploadTemplatesController {
  constructor($scope, $mdDialog, $mdToast, Upload, $interval, poller, $sce, smoothScroll, $location, $anchorScroll, HomeService, $stateParams, $state
  )
  {
    this.$scope = $scope;
    // this.homeService = HomeService;
    
    this.$mdDialog = $mdDialog;
    this.$mdToast = $mdToast;
    this.Upload = Upload;
    this.$stateParams = $stateParams;
    this.$interval = $interval;
    this.poller = poller;
    this.$sce = $sce;
    this.$state = $state
    
    this.smoothScroll = smoothScroll;
    this.$anchorScroll = $anchorScroll;
    this.$location = $location;
    this.homeService = HomeService;

    this.$scope.primaryCsvFiles = [];
    this.selectedItem = '';

    
  }

  uploadPrimaryFile = files => {
    console.log(files);
    let validExt = ["jpg", "jpeg", "png", "webp", "bmp",'pdf'];
      files.forEach(img => {
        let ext = img.name.split(".").pop(-1);
        if (validExt.indexOf(angular.lowercase(ext)) != -1) {
          this.$scope.primaryCsvFiles.push(img);
          }
          else {
            this.$mdToast
            .show(
              this.$mdToast
                .simple({
                  parent: angular.element(".upload-temp-wrapper")
                })
                .hideDelay(10000)
                .textContent(
                  `Please upload proper image/Pdf`
                )
                .capsule(true)
                .action("Got It!")
                .highlightClass("md-accent md-raised no-caps")
                .highlightAction(true)
                .toastClass("custom-toast-2")
                .position("top center")
            )
            .catch(err => {
              console.log('err',err);
            });
           files = [];
          //  this.$scope.primaryCsvFiles = []
          }
      });
      console.log('file-array',this.$scope.primaryCsvFiles);
  };

  removePrimaryFile = x => {
    // this.$scope.primaryCsvError = false;
    this.$scope.primaryCsvFiles.splice(x, 1);
  };

  /*** getting suggestion for template name */
  
  searchTemplate = (query) => {
    if(query != undefined) {
      return this.homeService.getTemplateNames(query).then((resp) => {
        this.$scope.templatesNameList = resp.data.data;
        console.log(this.$scope.templatesNameList);
        return this.$scope.templatesNameList;
      });
    }
  }

  onTemplateSelect = (item) => {
    console.log(item);
    this.$scope.selectedTempName = item;
  }

  submitTemplates = (ev) => {
    let _this = this;
    var confirm = this.$mdDialog.confirm()
    .title('Please Confirm')
    .textContent('')
    .ariaLabel('Submit')
    .targetEvent(ev)
    .ok('ok')
    .cancel('Cancel');

    this.$mdDialog.show(confirm).then(() => {
      console.log(this.$scope.primaryCsvFiles);
      var tem_id = this.$scope.selectedTempName.id;
      let data = new FormData();
      angular.forEach(this.$scope.primaryCsvFiles, function (value, key) {
        data.append('file', value);
      },this);
      
    this.homeService
      .submitTemplateFormData(data,tem_id)
      .then(resp => {
      if(resp.status === 200) {
        this.isMultiple = resp.data.is_multiple;
        console.log(this.isMultiple);
        // if(this.isMultiple == true){
        //   this.$state.go('history');
        // }
        // else {
        //   this.batchId = resp.data.batch_id;
        //   this.trackId = resp.data.tracking_id;
        //   console.log(tem_id);
        //   var obj = {id: tem_id,
        //     b_id: this.batchId,
        //     t_id: this.trackId}
        //   this.$state.go('template-predictions',{obj});
        // }
        this.$state.go('history');
        this.$mdDialog.hide();
      }
      })
      .catch(err => {
        console.log(err);
        this.$mdToast.show(
          this.$mdToast
            .simple()
            .textContent(`${err.data.data}`)
            .action("Ok")
            .highlightClass("md-warn")
            .highlightAction(true)
            .hideDelay(5000)
            .position("top right")
        );
      });
    }, () => {    
        });
      
  }


  resetInput = () => {
    this.$scope.uploadFormMultiple.$setPristine(true);
    this.$scope.uploadFormMultiple.$setUntouched(true);
    this.$scope.content = {};
    this.$scope.primaryCsvFiles = [];
    this.selectedItem = '';
    this.tempInitial = '';
    this.$scope.selectedTempName = '';
    
  };

  
}

UploadTemplatesController.$inject = [
  "$scope",
  "$mdDialog",
  "$mdToast",
  "Upload",
  "$interval",
  "poller",
  "$sce",
  "smoothScroll",
  "$location",
  "$anchorScroll",
  "HomeService",
  "$stateParams",
  "$state"
];
