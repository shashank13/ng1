import uiRouter from '@uirouter/angularjs';
import 'angular-material-data-table';
import ngFileUpload from 'ng-file-upload';

import './upload-templates.scss';
import '../../..//node_modules/ng-smooth-scroll/dist/angular-smooth-scroll.min.js';
import '../../../node_modules/angular-material-data-table/dist/md-data-table.css';

import uploadTemplatesConfig from './upload-templates.config';

import UploadTemplatesController from './upload-templates.controller';

// Services
import HomeService from '../services/home.service';

export default angular.module('app.uploadTemplates', [uiRouter,'md.data.table',ngFileUpload, HomeService])
                      .config(uploadTemplatesConfig)
                      .controller('UploadTemplatesController', UploadTemplatesController)
                      .name;