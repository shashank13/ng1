export default function config($stateProvider) {
  $stateProvider
    .state('upload-templates', {
      url: '/upload-templates',
      template: require('./upload-templates.html'),
      controller: 'UploadTemplatesController',
      controllerAs: 'uploadTemplatesCtrl',
      authenticate: true,
      resolve:{
        "check":['$window','$state', function($window,$state){
          if($window.localStorage.getItem('username')== null) {
            $state.go('login');
          } 
        }]
      }
    });
}

config.$inject = ['$stateProvider'];