import UrlHelper from "../helper/helper";

class DetailService extends UrlHelper {
  constructor($http, appConstants) {
    super(appConstants);
    this.$http = $http;
  }

  getDetail = (trackingId) => {
    return this.$http({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.DETAIL.GET_DETAIL,'/',trackingId])
    })
  }
}
export default angular.module('app.DetailService', [])
                      .service('DetailService', DetailService)
                      .name;

DetailService.$inject = ['$http', 'appConstants'];