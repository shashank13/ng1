import UrlHelper from "../helper/helper";

class TimelineService extends UrlHelper {
  constructor($http, appConstants) {
    super(appConstants);
    this.$http = $http;
  }


  // getTableData = (query, filter) => {
  //   return this.$http({
  //     method: 'POST',
  //     url: this.urlJoin([this.appConstants.ENDPOINT.TIMELINE.TIMELINE]),
  //     params: {
  //       page: query.page,
  //       size: query.size,
  //     },
  //     data: filter
  //   })
  // }

  deleteTemplate = (id) => {
    return this.$http({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.TIMELINE.DELETE_TEMP]),
      params: {
       tem_id: id
      }
    })
  }

  getTableData = (limit,page) => {
    return this.$http({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.TIMELINE.GET_TABLE_DATA]),
      params: {
        page: page,
        limit: limit
      }
    })
  }

  getTempTableData = (limit,page) => {
    return this.$http({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.TEMPLATES.GET_TABLE_DATA]),
      params: {
        page: page,
        limit: limit
      }
    })
  }

  cancelJob = (id) => {
    console.log('service',id);
    return this.$http({
      // headers: {'Content-Type': undefined},
      method: 'POST',
      // transformRequest: angular.identity,
      headers: {'Content-Type': 'application/json'},
      url: this.urlJoin([this.appConstants.ENDPOINT.TIMELINE.CANCEL_PROCESS]),
      data: {
        job_id: id
      }
    })
  }


}

export default angular.module('app.TimelineService', [])
  .service('TimelineService', TimelineService)
  .name;

TimelineService.$inject = ['$http', 'appConstants'];