import UrlHelper from "../helper/helper";

class HomeService extends UrlHelper {
  constructor($http, appConstants,upload,$resource) {
    super(appConstants);
    this.$http = $http;
    this.upload = upload;
    this.$resource = $resource;
  }


  sendAttr = (radio,id) => {
    console.log('send-data',radio);
    console.log('send-id',id);
    return this.$http ({
      method:'POST',
      // transformRequest: angular.identity,
      // headers: {'Content-Type': undefined},
      headers: {'Content-Type': 'application/json'},
      url: this.urlJoin([this.appConstants.ENDPOINT.HOME.SEND_ATTR_DATA]),
      data: {
        tracking_id: id,
        product_type: JSON.parse(radio),
      }
    })
  }


  submitFormData = (data) => {
    console.log('service-data',data);
    return this.$http({
      // transformRequest: angular.identity,
      headers: {'Content-Type': undefined},
      transformRequest: angular.identity,
      // headers: {'Content-Type': 'application/json'},
      method: 'POST',
      url: this.urlJoin([this.appConstants.ENDPOINT.HOME.FORM_SUBMIT]),
      // data: encodeURIComponent(JSON.stringify(data))
      data: data
    })
  }

  getTemplateNames = (query) => {
    return this.$http ({
      method:'POST',
      // transformRequest: angular.identity,
      // headers: {'Content-Type': undefined},
      headers: {'Content-Type': 'application/json'},
      url: this.urlJoin([this.appConstants.ENDPOINT.HOME.GET_TEMPLATE_LIST]),
      data: {
        template_name: query
      }
    })
  }

  submitTemplateFormData = (file,tem_id) => {
    console.log('data-service',file,tem_id);
    return this.$http ({
      method:'POST',
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined},
      // headers: {'Content-Type': 'application/json'},
      url: this.urlJoin([this.appConstants.ENDPOINT.HOME.SUBMIT_TEMPLATE_FORM]),
        params: {
          tem_id : tem_id,
        },
        data: file
        
    })
  }

  getTemplatePredictions = (page,limit,batch_id,tem_id) => {
    return this.$http({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.HOME.GET_TEMP_PREDICTION]),
      params: {
        page: page,
        limit: limit,
        batch_id: batch_id,
        tem_id: tem_id
      }
    })
  }

  //getting single template predictions
  getSingleTemplatePredictions = (track_id,batch_id,tem_id) => {
    return this.$http({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.HOME.GET_SINGLE_TEMP_PREDICT]),
      params: {
        tracking_id: track_id,
        batch_id: batch_id,
        tem_id: tem_id
      }
    })
  }


}

export default angular.module('app.HomeService', [])
                      .service('HomeService', HomeService)
                      .name;

HomeService.$inject = ['$http', 'appConstants', 'Upload','$resource'];