import UrlHelper from "../helper/helper";
import { userInfo } from "os";

class UserService extends UrlHelper {
  constructor($http, appConstants) {
    super(appConstants);
    this.$http = $http;
  }


  setEnvironment = (env) => {
    this.environment = env;
  }

  getEnvironment = () => {
    return this.environment;
  } 

  setData = (data) => {
    this.userData = data;
  }

  getData = () => {
    return this.userData;
  }

  loginUser = (userInfoData) => {
    console.log('s',userInfoData);
    return this.$http({
      method: 'POST',
      url: this.urlJoin([this.appConstants.ENDPOINT.USER.LOGIN]),
      data: userInfoData
    });
  }

  me = () => {
    return this.$http({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.USER.ME])
    });
  }

  updateAccountInfo = (email, fname, lname) => {
    return this.$http({
      method: 'POST',
      url: this.urlJoin([this.appConstants.ENDPOINT.USER.UPDATE]),
      data: {
        first_name: fname,
        last_name: lname,
        email: email
      }
    })
  }

  resetPassword = (pwd) => {
    return this.$http ({
      method: 'POST',
      url: this.urlJoin([this.appConstants.ENDPOINT.USER.RESET_PASSWORD]),
      data: pwd
    })
  }

  
  logout = () => {
    return this.$http({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.USER.LOGOUT])
    });
  }
}

export default angular.module('app.UserService', [])
                      .service('UserService', UserService)
                      .name;

UserService.$inject = ['$http', 'appConstants'];