import UrlHelper from "../helper/helper";

class AdminService extends UrlHelper {
  constructor($http, appConstants) {
    super(appConstants);
    this.$http = $http;
  }

  getListOfUsers = (query) => {
    return this.$http ({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.ADMIN.LIST_OF_USERS]),
      params: {
        page: query.page,
        size: query.size
      }
    })
  }

  createUser = (user) => {
    return this.$http ({
      method: 'POST',
      url: this.urlJoin([this.appConstants.ENDPOINT.ADMIN.REGISTER_USER]),
      data: user
    })
  }

  getRoles = () => {
    return this.$http ({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.ADMIN.GET_ROLES]),
    })
  }

  getCompanies = () => {
    return this.$http ({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.ADMIN.GET_COMPANIES]),
    })
  }

  deleteUser = (id) => {
    return this.$http ({
      method: 'GET',
      url: this.urlJoin([this.appConstants.ENDPOINT.ADMIN.DELETE_USER]),
      params: {
        userId: id
      }
    })
  }

  addRole = (chip,username) => {
    return this.$http ({
      method: 'POST',
      url: this.urlJoin([this.appConstants.ENDPOINT.ADMIN.ADD_ROLE]),
      data: {
        username: username,
        role: chip
      }
    })
  }

  removeRole = (chip,username) => {
    return this.$http ({
      method: 'POST',
      url: this.urlJoin([this.appConstants.ENDPOINT.ADMIN.REMOVE_ROLE]),
      data: {
        username: username,
        role: chip
      }
    })
  }

  checkUsernameValidity = (name) => {
    return this.$http ({
      method: 'POST',
      url: this.urlJoin([this.appConstants.ENDPOINT.ADMIN.USERNAME_VALIDITY]),
      data: {
        username: name
      }
    })
  }


}

export default angular.module('app.AdminService', [])
                      .service('AdminService', AdminService)
                      .name;

AdminService.$inject = ['$http', 'appConstants'];