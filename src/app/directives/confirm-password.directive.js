import angular from 'angular';

function confirmPassword() {
  return {
    require: 'ngModel',
    scope: {
      password: '=confirmPassword'
    },
    link: (scope, element, attr, ngModel) => {
      console.log(scope);
      console.log(element);
      console.log(attr);
      console.log(ngModel);
      ngModel.$validators.checkPassword = function(val) {
        return val == scope.password;
      }
      scope.$watch("password", () => {
        ngModel.$validate();
      })
    }
  }
}

export default angular.module('directive.validators', [])
  .directive('confirmPassword', confirmPassword)
  .name;
