import angular from "angular";
import "slick-carousel";

function slickCarouselEL(smoothScroll) {
  return {
    restrict: "A",
    transclude: false,
    link: function(scope, element) {
      if (scope.$last) {
        $(element.parent()).slick({
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 4,
          autoplay: true,
          dots: true,
          arrows: false,
          autoplaySpeed: 4000,
          responsive: [
            {
              breakpoint: 400,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                dots: true
              }
            },
            {
              breakpoint: 900,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
                dots: true
              }
            },
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4
              }
            }
          ]
        });
      }
    }
  };
}

export default angular
  .module("app.slickCarouselEl", [])
  .directive("slickCarouselEl", slickCarouselEL).name;

slickCarouselEL.$inject = ["smoothScroll"];
