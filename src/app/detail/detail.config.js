export default function config($stateProvider) {
  $stateProvider
    .state('detail', {
      url: '/detail/:trackingId',
      template: require('./detail.html'),
      controller: 'DetailController',
      controllerAs: 'detailCtrl',
      authenticate: true,
      resolve:{
        "check":['$window','$state', function($window,$state){
          if($window.localStorage.getItem('username')== null) {
            $state.go('login');
          } 
        }]
      }
    });
}

config.$inject = ['$stateProvider'];