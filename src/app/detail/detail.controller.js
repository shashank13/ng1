export default class DetailController {
  constructor($state, $scope, $mdDialog, $mdToast, DetailService, $sce) {
    this.$scope = $scope;
    this.$scope.selectedUsers = [];
    this.$mdDialog = $mdDialog;
    this.$mdToast = $mdToast;
    this.$sce = $sce;
    this.$state = $state;
    this.detailService = DetailService;
    this.trackingId = this.$state.params.trackingId;
    this.$scope.makeFieldsEditable = false;
    this.getDetail(this.trackingId);
    this.showNonePredictions = false;
  }

  magnifyImg = (src, index) => {
    this.$scope.divIndex = index;
    this.$scope.largeImgSrc = src;
    document.getElementById("div".concat(this.$scope.divIndex)).style.border =
      "2px solid #31b0de";
  };

  unFocusDiv = index => {
    document.getElementById("div".concat(this.$scope.divIndex)).style.border =
      "none";
  };

  getDetail = trackingId => {
    this.detailService.getDetail(trackingId).then(resp => {
      this.$scope.detail = resp.data;
      this.mapNonePrediction(this.$scope.detail.output.attributes);
      this.$scope.description = this.$sce.trustAsHtml(
        this.$scope.detail.output.description
      );
      // this.$scope.longDesc = this.$sce.trustAsHtml(this.$scope.detail.output.long_description);
      // this.$scope.attributes = this.$scope.detail.output.attributes;
    });
  };

  mapNonePrediction = attributes => {
    if (attributes) {
      this.$scope.nonePredictions = attributes.filter(
        attr => attr.result[0].prediction === "None"
      );
      this.$scope.definitePredictions = attributes.filter(
        attr => attr.result[0].prediction !== "None"
      );
    }
  };

  // showNonePredictions = () => {
  //   this.showNonePredictions = true;
  // }

  editTitle = (title, ev) => {
    this.$mdDialog.show({
      controllerAs: "productTitle",
      template: require("../components/templates/edit-product-title.html"),
      parent: angular.element(document.body),
      bindToController: true,
      preserveScope: true,
      targetEvent: ev,
      clickOutsideToClose: true,
      escapeToClose: true,
      locals: {
        title: title
      },
      controller: [
        "$scope",
        "$mdDialog",
        "DetailService",
        function($scope, $mdDialog, DetailService) {
          $scope.$mdDialog = $mdDialog;
          $scope.detailService = DetailService;
          // this.$scope.detail = detail;
          $scope.closeDialog = () => {
            $scope.$mdDialog.hide();
          };
        }
      ]
    });
  };

  editDescription = (shortDesc, longDesc, ev) => {
    this.$mdDialog.show({
      controllerAs: "productDesc",
      template: require("../components/templates/edit-product-desc.html"),
      parent: angular.element(document.body),
      bindToController: true,
      preserveScope: true,
      targetEvent: ev,
      clickOutsideToClose: true,
      escapeToClose: true,
      locals: {
        shortDesc: shortDesc,
        longDesc: longDesc
      },
      controller: [
        "$scope",
        "$mdDialog",
        "DetailService",
        function($scope, $mdDialog, DetailService) {
          $scope.$mdDialog = $mdDialog;
          $scope.detailService = DetailService;
          // this.$scope.detail = detail;
          $scope.closeDialog = () => {
            $scope.$mdDialog.hide();
          };
        }
      ]
    });
  };

  editAttribute = (attribute, ev) => {
    this.$mdDialog.show({
      controllerAs: "productAttr",
      template: require("../components/templates/edit-product-attributes.html"),
      parent: angular.element(document.body),
      bindToController: true,
      preserveScope: true,
      targetEvent: ev,
      clickOutsideToClose: true,
      escapeToClose: true,
      locals: {
        attribute: attribute
      },
      controller: [
        "$scope",
        "$mdDialog",
        "DetailService",
        function($scope, $mdDialog, DetailService) {
          $scope.$mdDialog = $mdDialog;
          $scope.detailService = DetailService;
          // this.$scope.detail = detail;
          $scope.closeDialog = () => {
            $scope.$mdDialog.hide();
          };
        }
      ]
    });
  };
}

DetailController.$inject = [
  "$state",
  "$scope",
  "$mdDialog",
  "$mdToast",
  "DetailService",
  "$sce"
];
