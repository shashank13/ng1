import 'angular-material-data-table';

import './detail.scss';
import '../../../node_modules/angular-material-data-table/dist/md-data-table.css';

import detailConfig from './detail.config';

import DetailController from './detail.controller';

// Services
import DetailService from '../services/detail.service';

export default angular.module('app.detail', ['md.data.table',DetailService])
                      .config(detailConfig)
                      .controller('DetailController', DetailController)
                      .name;