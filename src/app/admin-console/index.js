import uiRouter from '@uirouter/angularjs';
import 'angular-material-data-table';

import './admin-console.scss';
import '../../../node_modules/angular-material-data-table/dist/md-data-table.css';

import adminConfig from './admin-console.config';

import AdminController from './admin-console.controller';

// Services
import AdminService from '../services/admin.service';

export default angular.module('app.admin-console', [uiRouter, 'md.data.table', AdminService])
                      .config(adminConfig)
                      .controller('AdminController', AdminController)
                      .name;