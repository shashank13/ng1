export default function config($stateProvider) {
  $stateProvider
    .state('admin-console', {
      url: '/admin-console',
      template: require('./admin-console.html'),
      controller: 'AdminController',
      controllerAs: 'admin',
      authenticate: true,
      resolve:{
        "check":['$window','$state',function($window,$state){
            if($window.localStorage.getItem('username')== null) {
              $state.go('login');
            } else {   
              if($window.localStorage.getItem('isSuperAdmin') == "true"){ 
                  $state.go('admin-console');                    
              } else {
                  $state.go('dashboard');   
              }
            }
        }]
      }
    });
}

config.$inject = ['$stateProvider'];