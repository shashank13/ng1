export default class AdminController {
  constructor($scope, AdminService, $mdDialog, $mdToast) {
    this.$scope = $scope;
    this.adminService = AdminService;
    this.$mdDialog = $mdDialog;
    this.$mdToast = $mdToast;
    this.$scope.selectedProducts = [];
    this.$scope.tabIndex = 0;
    this.$scope.query = {
      page: 1,
      size: 5
    };
    this.getListOfUser(this.$scope.query);
  }

  //Getting list of existing users with pagination

  getListOfUser = (query) => {
    if(!query) {
      query = this.$scope.query;
    }
    this.adminService.getListOfUsers(query).then(resp => {
      this.getUserTableData(resp);
    })
  }

  getRequiredDetails = () => {
    this.getRoles();
    this.getCompanies();
  }


  getUserTableData = (response) => {
    this.$scope.userList = response.data.data.page_data
    this.$scope.query = {
      page: response.data.data.page,
      size: response.data.data.size,
      total_pages: response.data.data.total_pages,
      total_items: response.data.data.total_items
    };
  }

  /** paginate user Table */

  paginateUserTable = () => {
    this.adminService.getListOfUsers(this.$scope.query).then(response => {
      this.getUserTableData(response);
    }).catch(err => {
    });
  }


  getRoles = () => {
    this.adminService.getRoles().then(resp => {
      this.$scope.userRoles = resp.data.data;
    })
  }

  getCompanies = () => {
    this.adminService.getCompanies().then(resp => {
      this.$scope.companies = resp.data.data;
    })
  }

  createUser = (user) => {
    this.$scope.createUserSubmitted = true;
    this.adminService.createUser(user).then(resp => {
      this.$scope.createUserSubmitted = false;
      this.$mdDialog.show(
        this.$mdDialog.alert()
          .parent(angular.element(document.body))
          .clickOutsideToClose(true)
          .title('Success!!')
          .textContent('User Created successfully.')
          .ariaLabel('User Alert')
          .ok('Got it!')
      );
      this.refreshUserCreationForm();
      this.changeTab();
    }).catch(err => {
      this.$scope.createUserSubmitted = false;
      this.$mdToast.show(
        this.$mdToast.simple()
          .textContent(`${err.data.data}`)
          .action('Ok')
          .highlightClass('md-warn')
          .highlightAction(true)
          .hideDelay(5000)
          .position('top right')
      )
    })
  }

  changeTab = () => {
    this.$scope.tabIndex = 1;
  }

  refreshUserCreationForm = () => {
    this.$scope.create = {};
    this.$scope.createUser.$setPristine(true);
    this.$scope.createUser.$setUntouched(true);
    this.$scope.usernameValidityStatus = null;
  }

  deleteUser = (id) => {
    this.adminService.deleteUser(id).then(resp => {
      this.getListOfUser({
        page: 1,
        size: 5
      });
    })
  }


  addRole = (chip, username) => {
    this.adminService.addRole(chip,username).then(resp => {
      this.$mdDialog.show(
        this.$mdDialog.alert()
          .parent(angular.element(document.body))
          .clickOutsideToClose(true)
          .title('Success!')
          .textContent('Role has been successfully assigned to the user.')
          .ariaLabel('User Alert')
          .ok('Got it!')
      )
    }).catch(err => {
      this.$mdDialog.show(
        this.$mdDialog.alert()
          .parent(angular.element(document.body))
          .clickOutsideToClose(true)
          .title('ERROR!')
          .textContent(`${err.data.data}`)
          .ariaLabel('User Alert')
          .ok('Got it!')
      )
    })
  }

  removeRole = (chip, username) => {
    this.adminService.removeRole(chip,username).then(resp => {
      this.$mdDialog.show(
        this.$mdDialog.alert()
          .parent(angular.element(document.body))
          .clickOutsideToClose(true)
          .title('Success!')
          .textContent('Role has been removed successfully.')
          .ariaLabel('User Alert')
          .ok('Got it!')
      )
    }).catch(err => {
      this.$mdDialog.show(
        this.$mdDialog.alert()
          .parent(angular.element(document.body))
          .clickOutsideToClose(true)
          .title('ERROR!')
          .textContent(`${err.data.data}`)
          .ariaLabel('User Alert')
          .ok('Got it!')
      )
    })
  }

  checkUsernameValidity = (username) => {
    this.adminService.checkUsernameValidity(username).then(resp => {
      this.$scope.usernameValidityStatus = resp.data.data;
    })
  }
}

AdminController.$inject = ['$scope', 'AdminService', '$mdDialog', '$mdToast'];