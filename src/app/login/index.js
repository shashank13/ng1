// Modules
import angular from 'angular';
import uiRouter from '@uirouter/angularjs';

// Styles
import './login.scss';

// Routes
import loginConfig from './login.config';

// Services
import UserService from '../services/user.service';

// Controller
import LoginController from './login.controller';

export default angular.module('app.login', [uiRouter, UserService])
  .config(loginConfig)
  .controller('LoginController', LoginController)
  .name
