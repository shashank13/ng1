LoginConfig.$inject = ['$stateProvider'];

export default function LoginConfig($stateProvider) {
  $stateProvider
    .state('login', {
      url: '/',
      template: require('./login.html'),
      controller: 'LoginController',
      controllerAs: 'login',
      authenticate: false
    });
}
