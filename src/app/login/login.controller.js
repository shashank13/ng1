export default class LoginController {
  constructor($scope, UserService, $mdToast, $state, $window, $location) {
    this.userService = UserService;
    this.$scope = $scope;
    this.$location = $location;
    this.$mdToast = $mdToast;
    this.$state = $state;
    this.$window = $window;
    this.loginBtnDisabled = false;  
  }

  authenticateUser = (userInfo) => {
    console.log(userInfo);
    this.userService.loginUser(userInfo).then(resp => {
      this.userService.me().then(resp => {
        if (resp.status == 200) {
          let username = resp.data.username;
          let company = resp.data.company_name;
          this.$window.localStorage.setItem("username", username);
          this.$window.localStorage.setItem("company",company);
          this.userName = this.$window.localStorage.getItem("username");
          this.$state.go("upload-templates");
        }
      });
      
    }).catch(err => {
      this.$mdToast.show(
        this.$mdToast.simple()
          .textContent(err.data.data)
          .hideDelay(5000)
          .highlightAction(true)
          .action('Ok')
          .highlightClass('md-warn')
          .position('top right')
      );
    });
  }
}

LoginController.$inject = ['$scope', 'UserService', '$mdToast', '$state', '$window','$location'];