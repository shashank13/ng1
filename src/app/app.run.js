export default function appRun($state, $rootScope, UserService, $mdDialog) {

    UserService.me().then(resp => {
        $rootScope.userInfo = resp.data.data; 
        UserService.setData($rootScope.userInfo);
          if ( $state.current.name == 'login') {
            $state.go('dashboard');
          } 
          else {
            $state.go($state.current.name)
          }
      }).catch( err => {
        if ( $state.current.authenticate ) {
          $mdDialog.show(
            $mdDialog.alert()
              .parent(angular.element(document.body))
              .clickOutsideToClose(true)
              .title('Oops!!')
              .textContent('Not logged in, Please login to continue.')
              .ariaLabel('User Alert')
              .ok('Got it!')
          )
          $state.go('login');
        }
      });
}

appRun.$inject = ['$state','$rootScope','UserService','$mdDialog'];