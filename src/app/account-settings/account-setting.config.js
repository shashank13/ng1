export default function config($stateProvider) {
  $stateProvider
    .state('account-settings', {
      url: '/account-settings',
      template: require('./account-setting.html'),
      controller: 'AccountController',
      controllerAs: 'account',
      resolve:{
        "check":['$window','$state', function($window,$state){
          if($window.localStorage.getItem('username')== null) {
            $state.go('login');
          } 
        }]
      },
      authenticate: true
    });
}

config.$inject = ['$stateProvider'];