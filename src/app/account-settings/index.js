import uiRouter from '@uirouter/angularjs';

import './account-setting.scss';

import accountConfig from './account-setting.config';

import AccountController from './account-setting.controller';

import ValidatorDirective from '../directives/confirm-password.directive'

export default angular.module('app.account-setting', [uiRouter, ValidatorDirective])
                      .config(accountConfig)
                      .controller('AccountController', AccountController)
                      .name;