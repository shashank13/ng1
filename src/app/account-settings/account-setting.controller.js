export default class AccountController {
  constructor($scope, $state, UserService, $mdDialog, $mdToast) {
    this.$scope = $scope;
    this.$state = $state;
    this.userService = UserService;
    this.$mdDialog = $mdDialog;
    this.$mdToast = $mdToast;
    this.userService.me().then(resp => {
      this.$scope.accountInfo = resp.data.data;
    })
  }

  updateAccountInfo = (email,fname,lname) => {
    this.$scope.updateReqInProcess = true;
    this.userService.updateAccountInfo(email,fname,lname).then(resp => {
      this.$scope.updateReqInProcess = false;
      this.$mdToast.show(
        this.$mdToast.simple()
          .textContent('Detail updated successfully!')
          .action('Ok')
          .highlightClass('md-success')
          .highlightAction(true)
          .hideDelay(5000)
          .position('top right')
      )
    }).catch(err => {
      this.$scope.updateReqInProcess = false;
      this.$mdToast.show(
        this.$mdToast.simple()
          .textContent(`${err.data.data}`)
          .action('Ok')
          .highlightClass('md-warn')
          .highlightAction(true)
          .hideDelay(5000)
          .position('top right')
      )
    })
  }

  refreshUserPasswordForm = () => {
    this.$scope.passwordInfo = {};
    this.$scope.changePasswordForm.$setPristine(true);
    this.$scope.changePasswordForm.$setUntouched(true);
  }

  resetPassword = (pwdInfo) => {
    this.userService.resetPassword(pwdInfo).then(resp => {
      this.$mdToast.show(
        this.$mdToast.simple()
          .textContent('Password updated successfully!')
          .action('Ok')
          .highlightClass('md-success')
          .highlightAction(true)
          .hideDelay(5000)
          .position('top right')
      );
      this.refreshUserPasswordForm();
      this.$state.go('login');
    }).catch(err => {
      this.$mdToast.show(
        this.$mdToast.simple()
          .textContent(`${err.data.data}`)
          .action('Ok')
          .highlightClass('md-warn')
          .highlightAction(true)
          .hideDelay(5000)
          .position('top right')
      )
    })
  }
  
}

AccountController.$inject = ['$scope', '$state', 'UserService', '$mdDialog', '$mdToast'];