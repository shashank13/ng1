timelineConfig.$inject = ['$stateProvider'];

export default function timelineConfig($stateProvider) {
  $stateProvider
    .state('history', {
      url: '/history',
      template: require('./timeline.html'),
      controller: 'TimelineController',
      controllerAs: 'timeline',
      resolve:{
        "check":['$window','$state', function($window,$state){
          if($window.localStorage.getItem('username')== null) {
            $state.go('login');
          } 
        }]
      },
      authenticate: true
    });
}
