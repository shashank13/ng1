export default class TimelineController {
  constructor(
    $scope,
    $state,
    TimelineService,
    $mdDialog,
    $mdToast,
    $timeout,
    $window,
    UserService
  ) {

    this.$scope = $scope;
    this.$state = $state;
    this.$mdDialog = $mdDialog;
    this.timelineService = TimelineService;
    this.userService = UserService;
    this.$mdToast = $mdToast;
    this.$timeout = $timeout;
    this.$window = $window;
    //toggle button boolean
    this.downloadBtn = false;

    // check for the startdate cannot be greater than the end date .
    this.$scope.dateValid = true;
    // this.$scope.type = {};
    // console.log('obj-type',this.$scope.type);

    this.$scope.inProg = false;

    this.$scope.tableData = [];

    // pagination
    this.$scope.current_page = 1;

    // limit the default total pages
    this.$scope.page_size = 10;

    this.getTableData(10, 1);

    // fetching the company name
    this.$scope.companyName = '';
    this.getCompanyName();
  }

  getCompanyName = () => {
      this.$scope.companyName = this.$window.localStorage.company; 
  }
  getTableData = (limit, page) => {
    console.log(this.$window.localStorage);
    console.log(page, limit);
    this.$scope.loadingData = true;
    this.timelineService.getTableData(limit, page).then(resp => {
      console.log("table-data", resp);
      if (resp.status == 200) {
        this.$scope.current_page = resp.data.data.page;
        this.$scope.tableData = resp.data.data.page_data;
        this.$scope.total_pages = resp.data.data.total_pages;
        this.$scope.loadingData = false;
        // angular.forEach(this.$scope.tableData, function (value, key) {
        //   if(value.status == 'SCHEDULED' || value.status == 'PROCESSING') {
        //     console.log('cancelable');
        //     this.$scope.cancelItem = true;
        //   }
        //   if(value.status == 'COMPLETED' || value.status == 'CANCELED') {
        //     console.log('not cancelable');
        //     this.$scope.cancelItem = false;
        //   }
        // },this);
      }
    });
  };

  expandFilter = () => {
    this.$scope.expandFilter = !this.$scope.expandFilter;
    if (!this.$scope.expandFilter) {
      this.$scope.launch = {};
      this.$scope.launch.start_date = null;
      this.$scope.launch.end_date = null;
    }
  };

  refreshMe = () => {
    this.$scope.content = {};
    this.$scope.dateValid = true;
    // this.$scope.launch.start_date = null;
    // this.$scope.launch.end_date = null;
    this.$scope.start_m = null;
    this.$scope.end_m = null;
    this.$scope.tableData = [];
    this.$scope.inProg = false;
    this.$scope.expandFilter = false;
    this.getTableData(10, 1); // Initial load page = 1(pass page no to function)
  };

  paginateHistoryData = move => {
    console.log(move);
    this.$scope.currentPage = parseInt(this.$scope.current_page);
    this.$scope.currentPage += move;
    this.getTableData(this.$scope.page_size, this.$scope.currentPage);
  };

  showPredictions = (ev, item) => {
    console.log(item);
    var obj = {
      b_id: item.batch_id,
      t_id: item.template_id
    };
    this.$state.go("template-predictions", {
      b_id: item.batch_id,
      t_id: item.template_id
    });
  };
}

TimelineController.$inject = [
  "$scope",
  "$state",
  "TimelineService",
  "$mdDialog",
  "$mdToast",
  "$timeout",
  "$window",
  "UserService"
];
