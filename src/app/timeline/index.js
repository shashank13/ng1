import angular from 'angular';
import uiRouter from '@uirouter/angularjs';

import './timeline.scss';
import '../../../node_modules/ng-material-datetimepicker/dist/angular-material-datetimepicker.min.js';
import '../../../node_modules/ng-material-datetimepicker/dist/material-datetimepicker.min.css';


//Filters
import TrustHtmlFilter from '../filters/trust-html.filter';
import StripHtmlFilter from '../filters/strip-html.filter';

//Accordion-MJ
import '../../../node_modules/accordion-js/dist/accordion.css';
import '../components/accordion-mj/accordion.js'

// Services
import TimelineService from '../services/timeline.service';

import timelineConfig from './timeline.config';

import TimelineController from './timeline.controller';

import hiwToggleDirective from "../directives/hiw-toggle.directive";

export default angular.module('app.timeline', ['ngMaterialDatePicker', uiRouter, TimelineService, TrustHtmlFilter, StripHtmlFilter,hiwToggleDirective])
    .config(timelineConfig)
    .controller('TimelineController', TimelineController)
    .name;