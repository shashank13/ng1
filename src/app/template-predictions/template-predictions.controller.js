import appConstants from '../app.constants';
export default class TemplatePredictionsController {
  constructor($scope, $mdDialog, $mdToast, Upload, $interval, poller, $sce, smoothScroll, $location, $anchorScroll, HomeService,$stateParams,$state)
  {
    this.$scope = $scope;
    // this.homeService = HomeService;
    
    this.$mdDialog = $mdDialog;
    this.$mdToast = $mdToast;
    this.Upload = Upload;
    this.homeService = HomeService;
    this.$stateParams = $stateParams;
    this.$state = $state;

    
    this.$interval = $interval;
    this.poller = poller;
    this.$sce = $sce;
    
    this.smoothScroll = smoothScroll;
    this.$anchorScroll = $anchorScroll;
    this.$location = $location;

  
    this.$scope.batchId= this.$state.params.b_id;
    this.$scope.temId = this.$state.params.t_id;
    this.$scope.current_page = 1;
    this.$scope.current_item = 1;
    this.$scope.page_size = 10;
    this.$scope.itemLimit=0;
    this.$scope.editAttributes = 0;

    this.$scope.isLoading = true;
    this.$scope.changeImgLoading = true;

    this.$scope.API_URL = appConstants.API_URL;
    this.$scope.showMe = false;
    this.$scope.predictionData;

    this.getTemplatePrediction(this.$scope.current_page,this.$scope.page_size,this.$scope.batchId,this.$scope.temId);

  }

  getTemplatePrediction = (page,limit,batch_id,tem_id) => {
    this.homeService.getTemplatePredictions(page,limit,batch_id,tem_id).then(resp => {
        if(resp.status == 200) {
        this.$scope.dataLoaded = true;
        this.$scope.isLoading = true;
        this.$scope.changeImgLoading = true;
        console.log('prediction',resp.data.data);
        this.$scope.current_page = resp.data.data.page;
        this.$scope.total_items = resp.data.data.total_items;
        this.$scope.total_pages = resp.data.data.total_pages;
        this.$scope.itemLimit=resp.data.data.size;

        this.$scope.imageList = resp.data.data.page_data;
        this.$scope.selectedTrackId = resp.data.data.page_data[0].tracking_id;
        this.$scope.selectedBatchId = resp.data.data.page_data[0].batch_id;
        this.$scope.selectedTemId = resp.data.data.page_data[0].tem_id;
        console.log(this.$scope.selectedTrackId);
        this.getSingleImgPrediction(this.$scope.selectedTrackId,this.$scope.selectedBatchId,this.$scope.selectedTemId);
        }
    })
  }

  paginateHistoryThumbData = move => {
    console.log(move);
    this.$scope.currentPage = parseInt(this.$scope.current_page);
    this.$scope.currentPage += move;
    this.$scope.current_item=(parseInt(this.$scope.itemLimit)*(parseInt(this.$scope.currentPage)-1))+1; // To set current item as first on each page
    this.getTemplatePrediction( this.$scope.currentPage,this.$scope.page_size,this.$scope.batchId, this.$scope.temId);
  };

  predictImage = (item,index) => {
    const i=index+1;
    this.$scope.current_item = i+(parseInt(this.$scope.current_page-1)*parseInt(this.$scope.itemLimit));
    this.$scope.isLoading = true;
    this.$scope.changeImgLoading = true;
    this.getSingleImgPrediction(item.tracking_id,item.batch_id,item.tem_id);
  }

  onEditAttributes = () => {
    this.$scope.editAttributes = 1;
    console.log('edit');
    console.log('Priting data captured');
    this.$scope.predictionData = this.$scope.singleImgPrediction;
    console.log(this.$scope.predictionData);
  }
  
  onSubmitEdit = (item) =>{
    this.$scope.editAttributes = 0;
    console.log('Updated Values');
    console.log(this.$scope.predictionData);
  }

  getSingleImgPrediction = (track_id,batch_id,tem_id) => {
    this.homeService.getSingleTemplatePredictions(track_id,batch_id,tem_id).then(resp => {
        if(resp.status == 200) {
          console.log(resp.data);
          this.$scope.isLoading = false;
          this.$scope.changeImgLoading = false;
          this.$scope.selectedImg = resp.data.data.image;
          this.$scope.singleImgPrediction = resp.data.data.predictions;
          console.log(this.$scope.singleImgPrediction);
        }
    })
  }
  // Edit Dialog============================================================================ Start

  editPredictionDialog = (ev,editableData) => {
    console.log(editableData);
    this.$mdDialog.show({
      controllerAs: "predictionDetails",
      template: require("../components/templates/edit-predictions.html"),
      parent: angular.element(document.body),
      bindToController: true,
      preserveScope: true,
      targetEvent: ev,
      clickOutsideToClose: true,
      escapeToClose: true,
      locals: {
        name:'Shashank'
      },
      controller: [
        "$scope",
        "$mdDialog",
        function($scope, $mdDialog) {
          $scope.$mdDialog = $mdDialog;
          this.$scope.editableData = editableData;
          $scope.closeDialog = () => {
            $scope.$mdDialog.hide();
          };
          $scope.saveData = (editableData) => {
            console.log('Got the data here');
            console.log(editableData);
            $scope.$mdDialog.hide();
          };
        }
      ]
    });
  };

  // Edit Dialog============================================================================ End

  showLargeImgPrediction = (selected_img,ev) => {
    this.$mdDialog.show({
      controllerAs: "zoomCtrl",
      template: require("../components/templates/zoom-prediction.html"),
      parent: angular.element(document.body),
      bindToController: true,
      targetEvent: ev,
      clickOutsideToClose: true,
      escapeToClose: true,
      locals: {
        largeDetailImgSrc: selected_img,
        API_URL: this.$scope.API_URL
      },
      controller: [
        "$scope",
        "$mdDialog",
        function($scope, $mdDialog) {
          $scope.$mdDialog = $mdDialog;
          $scope.closeDialog = () => {
            $scope.$mdDialog.hide();
          };
          $scope.zoomItNow = () => {
            $scope.zoomNow = true;
            let zoomedElementDiv = $(`#zoomedImageDiv`);
            let zoomedElement = $(`#zoomedImageDiv img`);
            if (
              angular.element(zoomedElement).hasClass("normal-broad-image-view")
            ) {
              angular
                .element(zoomedElement)
                .removeClass("normal-broad-image-view");
              angular.element(zoomedElement).addClass("enlarge-image-view");
              angular
                .element(zoomedElementDiv)
                .removeClass("zoom-remove-style");
              angular.element(zoomedElementDiv).addClass("zoom-add-style");
            } else if (
              angular.element(zoomedElement).hasClass("enlarge-image-view")
            ) {
              angular.element(zoomedElement).removeClass("enlarge-image-view");
              angular
                .element(zoomedElement)
                .addClass("normal-broad-image-view");
              angular.element(zoomedElementDiv).removeClass("zoom-add-style");
              angular.element(zoomedElementDiv).addClass("zoom-remove-style");
            }

            // ========================================
          };
          

        }
      ]
    });
  };

}

TemplatePredictionsController.$inject = [
  "$scope",
  "$mdDialog",
  "$mdToast",
  "Upload",
  "$interval",
  "poller",
  "$sce",
  "smoothScroll",
  "$location",
  "$anchorScroll",
  "HomeService",
  "$stateParams",
  "$state"
];
