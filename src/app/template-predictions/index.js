import uiRouter from '@uirouter/angularjs';
import 'angular-material-data-table';
import ngFileUpload from 'ng-file-upload';

import './template-predictions.scss';
import '../../..//node_modules/ng-smooth-scroll/dist/angular-smooth-scroll.min.js';
import '../../../node_modules/angular-material-data-table/dist/md-data-table.css';

// import "../../../node_modules/slick-carousel/slick/slick.css";

// import "../../../node_modules/slick-carousel/slick/slick.js";

import templatePredictionsConfig from './template-predictions.config';

import TemplatePredictionsController from './template-predictions.controller';

// Services
import HomeService from '../services/home.service';
// import slickCarousel from "../directives/slick-carousel.directive";

export default angular.module('app.template-predictions', [uiRouter,'md.data.table',ngFileUpload,HomeService])
                      .config(templatePredictionsConfig)
                      .controller('TemplatePredictionsController', TemplatePredictionsController)
                      .name;