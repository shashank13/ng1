export default function config($stateProvider) {
  $stateProvider
    .state('template-predictions', {
      url: '/template-predictions?:b_id?:t_id',
      template: require('./template-predictions.html'),
      controller: 'TemplatePredictionsController',
      controllerAs: 'templatePredictionsCtrl',
      authenticate: true,
      resolve:{
        "check":['$window','$state', function($window,$state){
          if($window.localStorage.getItem('username')== null) {
            $state.go('login');
          } 
        }]
      }
    });
}

config.$inject = ['$stateProvider'];