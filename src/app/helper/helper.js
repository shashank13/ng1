export default class Helper {
  constructor(appConstants) {
    this.appConstants = appConstants;
  }

  /**
   * Join all URL Constants
   * @param list
   * @return {string|*}
   */
  urlJoin = (list)  => {
    list.unshift(this.appConstants.API_URL);
    return list.join('');
  };
}

Helper.$inject = ['appConstants'];