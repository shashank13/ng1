import Helper from './helper';
import appConstants from '../app.constants';

describe('Class: Helper', () => {
  let helper;

  describe('# constructor', () => {
    it('should throw error when no argument is passed', () => {
      helper = new Helper();
      expect(helper.appConstants).toBeUndefined();
    });

    it('requires an object as an argument', () => {
      helper = new Helper(appConstants);
      expect(helper.appConstants).toBe(appConstants);
    });
  });

  describe('# urlJoin method', () => {
    it('should return a url path as a string', () => {
      let helper = new Helper(appConstants);
      let apiURL = helper.appConstants.API_URL;
      let sampleEndpoint = ['/api/endpoint'];
      expect(helper.urlJoin(sampleEndpoint)).toBe(`${apiURL}/api/endpoint`);
    });
  });

});