let appConstants = {
  'LABEL_APP_BASE_URL': LABEL_APP_BASE_URL,
  'API_URL': API_URL,
  'ENDPOINT': {
    'USER': {
      'LOGIN': '/api/users/login/',
      'ME': '/api/users/me/',
      'LOGOUT': '/api/users/logout/',
      'UPDATE': '/user/updateProfile',
      'RESET_PASSWORD': '/user/resetPassword'
    },
      'ADMIN': {
        'LIST_OF_USERS': '/user/completeList',
        'REGISTER_USER': '/user/register',
        'GET_ROLES': '/user/roles',
        'GET_COMPANIES':'/user/companies',
        'DELETE_USER': '/user/delete',
        'ADD_ROLE': '/user/assignRole',
        'REMOVE_ROLE': '/user/removeRole',
        'USERNAME_VALIDITY': '/user/check'
      },
      'HOME':{
        'FORM_SUBMIT': '/api/process/template/create/',
        'GET_TEMPLATE_LIST': '/api/process/template/search/',
        'SUBMIT_TEMPLATE_FORM': '/api/process/upload/',
        'GET_TEMP_PREDICTION': '/api/process/history/detail/',
        'GET_SINGLE_TEMP_PREDICT': '/api/process/prediction/'

      },
      'DETAIL':{
        'GET_DETAIL': '/process/detail',
        'GET_PREDICTIONS': '/api/process/get_attributes/',
        'GET_PREDICTIONS_ATTRS': '/api/process/product/detail'
      },
      'TIMELINE': {
        'TIMELINE': '/dashboard/filter/data',
        'GET_FILTER_PARAM': '/dashboard/filter/param',
        'GET_VALUES_FOR_SELECTED_ATTRIBUTE': '/dashboard/filter/param/attribute',
        'GET_TABLE_DATA': '/api/process/history/',
        'CANCEL_PROCESS': '/api/job/cancel-job/',
        'DELETE_TEMP': '/api/process/template/delete/'
      },
      'TEMPLATES': {
        'GET_TABLE_DATA': '/api/process/template/list/',
      },
      
  }
};

export default appConstants;