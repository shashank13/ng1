import angular from 'angular';

let unsafe = ['$sce', function($sce) {
    return $sce.trustAsHtml;
}];

export default angular.module('filter.unsafe', [])
  .filter('unsafe', unsafe)
  .name;
  
  

 