import angular from 'angular';

let htmlToPlaintext = () => {
    return function(text) {
      var frags = text.split('_');
      for (var i=0; i<frags.length; i++) {
        // console.log('i-elements',frags[i]);
        frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
      }
      return frags.join('.');
      };
};

export default angular.module('filter.stripHtml', [])
  .filter('stripHtml', htmlToPlaintext)
  .name;
  
  

 