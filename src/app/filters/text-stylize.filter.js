import angular from 'angular';

let txtStylize = () => {
  let str = [];
  return (input) => {
    if ( isNaN(input) ) {
      str = input.split('_').map( w => {
        return w[0].toUpperCase() + w.substr(1, w.length)
      });
      return str.join(" ");
    } else {
      return input;
    }
  }
};

export default angular.module('filter.txtStylize', [])
  .filter('txtStylize', txtStylize)
  .name;
