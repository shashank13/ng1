export default function appConfig($locationProvider, $mdThemingProvider, $httpProvider, $mdAriaProvider) {
  /* Base URL */
  $locationProvider.html5Mode(true);
  
  /* Custom theming for app */
  $mdThemingProvider
  .theme('default')
  .primaryPalette('blue')
  .accentPalette('teal')
  .warnPalette('red')
  .backgroundPalette('grey');

   /**
   * Http Interceptor
   */
  $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
  $httpProvider.defaults.withCredentials = true;
  $httpProvider.defaults.xsrfCookieName = 'csrftoken';
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
  $httpProvider.interceptors.push(function() {
    return {
      response: (response) => {
        $httpProvider.defaults.headers.common['X-CSRFToken'] = response.headers('X-CSRF');
        return response;
      }
    }
  });

  $mdAriaProvider.disableWarnings();
}

appConfig.$inject = ['$locationProvider', '$mdThemingProvider', '$httpProvider', '$mdAriaProvider'];