import FooterController from './footer.controller';

export default angular.module('app.footer', [])
  .component('footerEl', {
    controller: FooterController,
    controllerAs: 'footer',
    template: require('./footer.html')
  })
  .name