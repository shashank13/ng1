import TopMenuController from './top-menu.controller';

export default angular.module('app.TopMenu', [])
  .component('topMenu', {
    controller: TopMenuController,
    controllerAs: 'TopMenu',
    template: require('./top-menu.html')
  })
  .name