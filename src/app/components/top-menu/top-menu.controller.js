export default class TopMenuController {
  constructor($scope, userService, $state, $rootScope, $mdSidenav, $window) {
    this.userService = userService;
    this.$state = $state;
    this.$rootScope = $rootScope;
    this.$scope = $scope;
    this.$window = $window;
    userService.me().then(function(resp) {
      $scope.userData = resp.data.username;
      $scope.companyName = "MACNICA";
      $scope.logoImgUrl = resp.data.company_logo;
      if ($window.localStorage.getItem("username") == null) {
        $state.go("login");
      }
      // else {
      //   if ($scope.userData.roles.indexOf("SUPER_ADMIN") > -1) {
      //     $scope.isSuperAdmin = true;
      //     $window.localStorage.setItem('isSuperAdmin', true);
      //   } else {
      //     $window.localStorage.setItem('isSuperAdmin', false);
      //   }
      //   if ($scope.userData.roles.indexOf("USER") > -1) {
      //     $scope.isUser = true;
      //     $window.localStorage.setItem('isUser', true);
      //   } else {
      //     $window.localStorage.setItem('isUser', false);
      //   }
      // }
    });
  }

  logoutUser = () => {
    this.userService
      .logout()
      .then(resp => {
        this.$state.go("login");
        this.$window.localStorage.clear();
      })
      .catch(err => {
        // error
      });
  };
}

TopMenuController.$inject = [
  "$scope",
  "UserService",
  "$state",
  "$rootScope",
  "$mdSidenav",
  "$window"
];
