import uiRouter from '@uirouter/angularjs';
import 'angular-material-data-table';
import ngFileUpload from 'ng-file-upload';

import './home.scss';
import '../../..//node_modules/ng-smooth-scroll/dist/angular-smooth-scroll.min.js';
import '../../../node_modules/angular-material-data-table/dist/md-data-table.css';

import homeConfig from './home.config';

import HomeController from './home.controller';

// Services
import HomeService from '../services/home.service';

export default angular.module('app.home', [uiRouter, 'md.data.table', HomeService, ngFileUpload])
                      .config(homeConfig)
                      .controller('HomeController', HomeController)
                      .name;