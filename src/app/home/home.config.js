export default function config($stateProvider) {
  $stateProvider
    .state('dashboard', {
      url: '/home',
      template: require('./home.html'),
      controller: 'HomeController',
      controllerAs: 'home',
      authenticate: true,
      resolve:{
        "check":['$window','$state', function($window,$state){
          if($window.localStorage.getItem('username')== null) {
            $state.go('login');
          } 
        }]
      }
    });
}

config.$inject = ['$stateProvider'];