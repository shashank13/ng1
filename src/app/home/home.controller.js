import appConstants from '../app.constants';

export default class HomeController {
  constructor(
    $scope,
    HomeService,
    $mdDialog,
    $mdToast,
    Upload,
    $interval,
    poller,
    $sce,
    smoothScroll,
    $location,
    $anchorScroll,
    $state,
    $stateParams
  ) {
    this.$scope = $scope;
    this.homeService = HomeService;
    this.$mdDialog = $mdDialog;
    this.$mdToast = $mdToast;
    this.Upload = Upload;
    this.$scope.content = {};
    this.$scope.primaryCsvFiles = [];
    this.$interval = $interval;
    this.poller = poller;
    this.$sce = $sce;
    this.DOC_MAX = 1;
    this.smoothScroll = smoothScroll;
    this.$anchorScroll = $anchorScroll;
    this.$location = $location;
    this.$state = $state;
    this.$stateParams = $stateParams;
    
    this.$scope.angle = 0;

  }
  
  
  /*** primary Image  **** */

  uploadPrimaryFile = files => {
    console.log(files);
    this.$scope.angle = 0;
    // this.$scope.primaryCsvError = false;
    // let ext = files.name.split(".").pop(-1);
    let validExt = ["jpg", "jpeg", "png", "webp", "bmp"];
    // if (validExt.indexOf(angular.lowercase(ext)) == -1)
      files.forEach(img => {
        console.log(img.name);
        let ext = img.name.split(".").pop(-1);
        if (this.$scope.primaryCsvFiles.length < this.DOC_MAX && validExt.indexOf(angular.lowercase(ext)) != -1) {
            
          this.$scope.primaryCsvFiles.push(img);

          }
          else {
            this.$mdToast
            .show(
              this.$mdToast
                .simple({
                  parent: angular.element(".home-wrapper")
                })
                .hideDelay(10000)
                .textContent(
                  `Please upload proper image`
                )
                .capsule(true)
                .action("Got It!")
                .highlightClass("md-accent md-raised no-caps")
                .highlightAction(true)
                .toastClass("custom-toast-2")
                .position("top center")
            )
            .catch(err => {
              console.info("Other toast closed.");
            });
          }
      });
      console.log('file-array',this.$scope.primaryCsvFiles);
    
    // this.checkDocValidity(this.$scope.primaryCsvFiles);
  };

  removePrimaryFile = x => {
    // this.$scope.primaryCsvError = false;
    this.$scope.angle = 0;
    // let elem = document.getElementById('can-rotate');
    // document.getElementById('large-padding').style.padding = '0px';
    // elem.className = "";
    this.$scope.primaryCsvFiles.splice(x, 1);
  };

  // rotateImage = () => {
  //   let _this = this;
  //   // angular.forEach(this.$scope.primaryCsvFiles, function(step, i) {
  //   //     console.log(step);
  //   // },this);
  //   let container = document.getElementsByClassName('img-thumbnail-large');
  //   let elem = document.getElementById('can-rotate');
  //   this.$scope.angle = (this.$scope.angle + 90) % 360;
  //   console.log(this.$scope.angle);
  //   const width = elem.clientWidth;
  //   const height = elem.clientHeight;
  //   console.log('w',width,'h',height);
  //   elem.className = "rotate" + this.$scope.angle;
  //   function setScroll() {
  //     const scrollHeight = container[0].scrollHeight;
  //     const scrollWidth = container[0].scrollWidth;
  //     console.log(scrollWidth,scrollHeight);
  //     if(_this.$scope.angle === 90 || _this.$scope.angle === 270) {
  //       container[0].style.paddingTop= parseInt(width - scrollHeight) + 'px';
  //       container[0].style.paddingLeft= parseInt(height - scrollWidth) + 'px';
  //     } else {
  //       container[0].style.paddingTop = '0px';
  //     }
  //   }
  //   var event = new Event('image-rotate');
  //   elem.addEventListener('image-rotate', setScroll);
  //   elem.dispatchEvent(event);
  //   elem.removeEventListener('image-rotate', setScroll);
  //   console.log(elem);


  // }

  

  // Label Image
  // /**
  //  * 
  //  * @param event
  //  */
  submitForm = (content,ev) => {
    let _this = this;
    var confirm = this.$mdDialog.confirm()
    .title('Please Confirm')
    .textContent('')
    .ariaLabel('Submit')
    .targetEvent(ev)
    .ok('ok')
    .cancel('Cancel');

    this.$mdDialog.show(confirm).then(() => {
      let data = new FormData();
      data.append('image',this.$scope.primaryCsvFiles[0]);
      data.append('template_name',content.template_name);
      data.append('rotation_angle',this.$scope.angle);
    this.homeService
      .submitFormData(data)
      .then(resp => {
      if(resp.status === 200) {
        this.tem_id = resp.data.template_id
        console.log(this.tem_id);
      // this.$state.go('history');
      this.$mdDialog.hide();
      // window.location.href = 'http://localhost:8080/labelimg.html?id=' + this.tem_id;
      window.location.href = appConstants.LABEL_APP_BASE_URL + '?id=' + this.tem_id;
      }
      })
      .catch(err => {
        console.log(err);
        this.$mdToast.show(
          this.$mdToast
            .simple()
            .textContent(`${err.data.data}`)
            .action("Ok")
            .highlightClass("md-warn")
            .highlightAction(true)
            .hideDelay(5000)
            .position("top right")
        );
      });
    }, () => {    
        });
      
      
  };

  
  

  
  

  resetInput = () => {
    this.$scope.uploadForm.$setPristine(true);
    this.$scope.uploadForm.$setUntouched(true);
    this.$scope.uploadForm.template_name.$setValidity("required",true);
    this.$scope.content = {};
    this.$scope.primaryCsvFiles = [];
    this.$scope.dateValid = true;
    
    // $('.advance-wrap').removeClass('active').addClass('out');
  };

  
}

HomeController.$inject = [
  "$scope",
  "HomeService",
  "$mdDialog",
  "$mdToast",
  "Upload",
  "$interval",
  "poller",
  "$sce",
  "smoothScroll",
  "$location",
  "$anchorScroll",
  "$state",
  "$stateParams",
];
