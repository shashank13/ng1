import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import ngAnimate from 'angular-animate';
import ngMaterial from 'angular-material';
import ngCookies from 'angular-cookies';
import ngMessages from 'angular-messages';
import ngFileUpload from 'ng-file-upload';
import 'ng-smooth-scroll';
import ngPoller from 'angular-poller';
import ngResource from 'angular-resource';


// App Constants
import AppConstants from './app.constants';

// App Config
import AppConfig from './app.config';

// App Run
import AppRun from './app.run';

// App Components
import TopMenuComponent from './components/top-menu';
// import SideMenuComponent from './components/side-menu';
import FooterComponent from './components/footer';

// Modules
import HomeModule from './home';
import UploadTemplates from './upload-templates';
import TemplatePredictionModule from './template-predictions';
import LoginModule from './login';
// import AccountModule from './account-settings';
// import AdminModule from './admin-console';
import TimelineModule from './timeline';
import TemplatesModule from './templates';
// import DetailModule from './detail';

// Main Styles
import '../assets/styles/main.scss';
import '../../node_modules/angular-material/angular-material.scss'; // Angular Material
import '../../node_modules/animate.css/animate.min.css'; // animate.css

// datax.ai logo
import '../assets/img/favicon.ico';

angular.module('app', [ngMaterial, ngAnimate, uiRouter, ngCookies, ngMessages, ngFileUpload,'smoothScroll', ngPoller, ngResource, 
                  TopMenuComponent, FooterComponent, HomeModule, LoginModule, TimelineModule, TemplatesModule, UploadTemplates, TemplatePredictionModule])
      .constant('appConstants', AppConstants)
      .run(AppRun)
      .config(AppConfig);