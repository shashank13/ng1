import appConstants from '../app.constants';
export default class TemplatesController {
  constructor($scope, $state, TimelineService, $mdDialog, $mdToast, $timeout,$window) {
    this.$scope = $scope;
    this.$state = $state;
    this.$mdDialog = $mdDialog;
    this.timelineService = TimelineService;
    this.$mdToast = $mdToast;
    this.$timeout = $timeout;
    this.$window = $window;

      //toggle button boolean 
      this.downloadBtn = false;

  
      // check for the startdate cannot be greater than the end date .
      this.$scope.dateValid = true;
  
      // this.$scope.type = {};
      // console.log('obj-type',this.$scope.type);
  
      this.$scope.inProg = false;

      this.$scope.tableData = [];

      // pagination
      this.$scope.current_page = 1;
      

    // limit the default total pages
    this.$scope.page_size = 10;

    this.$scope.API_URL = appConstants.API_URL;
    this.getTableData(10,this.$scope.current_page );

  }


  getTableData = (limit,page) => {
    console.log(page,limit);
    this.$scope.loadingData = true;
    this.timelineService.getTempTableData(limit,page).then(resp =>{
      console.log('table-data',resp);
      if(resp.status == 200){
        this.$scope.current_page = resp.data.data.page;
        this.$scope.tableData = resp.data.data.page_data;
        this.$scope.total_pages = resp.data.data.total_pages;
        this.$scope.loadingData = false;
        // angular.forEach(this.$scope.tableData, function (value, key) {
        //   this.$scope.dynamicImgUrl = value.image_url;  
        //   console.log(this.$scope.dynamicImgUrl);
        // },this);
      }
    });
  }

  expandFilter = () => {
    this.$scope.expandFilter = !this.$scope.expandFilter;
    if (!this.$scope.expandFilter) {
      this.$scope.launch = {};
      this.$scope.launch.start_date = null;
      this.$scope.launch.end_date = null;
    }
  };

  deleteTemplate = (ev, item) => {
    let _this = this;
    var confirm = this.$mdDialog
      .confirm()
      .title("Are You Sure ?")
      .ariaLabel("Lucky day")
      .targetEvent(ev)
      .ok("Delete")
      .cancel("Cancel");

    this.$mdDialog.show(confirm).then(function() {
      _this.timelineService
        .deleteTemplate(item.id)
        .then(resp => {
          if (resp.status === 200) {
            _this.getTableData(_this.$scope.page_size, _this.$scope.current_page);
          }
        })
        .catch(err => {
          _this.$mdToast.show(
            _this.$mdToast
              .simple()
              .textContent(err.data.data)
              .action("Ok")
              .highlightClass("md-warn")
              .highlightAction(true)
              .hideDelay(5000)
              .position("top right")
          );
        });
    });
  }

  refreshMe = () => {
    this.$scope.content = {};
    this.$scope.dateValid = true;
    // this.$scope.launch.start_date = null;
    // this.$scope.launch.end_date = null;
    this.$scope.start_m = null;
    this.$scope.end_m = null;
    this.$scope.tableData = [];
    this.$scope.inProg = false;
    this.$scope.expandFilter = false;
    this.getTableData(10, 1); // Initial load page = 1(pass page no to function)
  }

  paginateHistoryData = move => {
    console.log(move);
    this.$scope.currentPage = parseInt(this.$scope.current_page);
    this.$scope.currentPage += move;
    this.getTableData(this.$scope.page_size, this.$scope.currentPage);
  };

}

TemplatesController.$inject = [
  "$scope",
  "$state",
  "TimelineService",
  "$mdDialog",
  "$mdToast",
  "$timeout",
  "$window"
];
