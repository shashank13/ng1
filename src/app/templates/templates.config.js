timelineConfig.$inject = ['$stateProvider'];

export default function timelineConfig($stateProvider) {
  $stateProvider
    .state('templates', {
      url: '/templates',
      template: require('./templates.html'),
      controller: 'TemplatesController',
      controllerAs: 'templatesCtrl',
      resolve:{
        "check":['$window','$state', function($window,$state){
          if($window.localStorage.getItem('username')== null) {
            $state.go('login');
          } 
        }]
      },
      authenticate: true
    });
}
