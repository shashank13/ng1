import angular from 'angular';
import uiRouter from '@uirouter/angularjs';

import './templates.scss';
import '../../../node_modules/ng-material-datetimepicker/dist/angular-material-datetimepicker.min.js';
import '../../../node_modules/ng-material-datetimepicker/dist/material-datetimepicker.min.css';


//Filters
import TrustHtmlFilter from '../filters/trust-html.filter';
import StripHtmlFilter from '../filters/strip-html.filter';

//Accordion-MJ
import '../../../node_modules/accordion-js/dist/accordion.css';
import '../components/accordion-mj/accordion.js'

// Clipboard
import "../../../node_modules/ngclipboard/dist/ngclipboard.min.js";

// Services
import TimelineService from '../services/timeline.service';

import templatesConfig from './templates.config';

import TemplatesController from './templates.controller';

import hiwToggleDirective from "../directives/hiw-toggle.directive";

export default angular.module('app.templates', ['ngMaterialDatePicker','ngclipboard', uiRouter, TimelineService, TrustHtmlFilter, StripHtmlFilter,hiwToggleDirective])
    .config(templatesConfig)
    .controller('TemplatesController', TemplatesController)
    .name;