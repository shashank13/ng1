const path = require('path');
require('dotenv').config();
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { BaseHrefWebpackPlugin } = require('base-href-webpack-plugin');

const ROOT_DIR = path.resolve(__dirname, '../');
const DIST_DIR = path.resolve(ROOT_DIR, 'dist');
const SRC_DIR = path.resolve(ROOT_DIR, 'src');
const APP_DIR = path.resolve(SRC_DIR, 'app');

// Check ENV (PROD or DEVELOPMENT)
const devMode = (process.env.NODE_ENV !== 'production');

let config = {
  entry: {
    app: path.resolve(APP_DIR, 'main.js'),
  },
  resolve: {
    modules: [
      'node_modules'
    ],
    extensions: ['.js', '.scss', '.css'],
    alias: {
      theme: path.resolve(ROOT_DIR, 'theme')
    },
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: [
          path.resolve(ROOT_DIR, 'node_modules')
        ],
        options: {
          presets: [
            "env"
          ],
          plugins: [
            "transform-class-properties"
          ]
        }
      },
      {
        test: /\.(png|ico|gif|svg|jpe?g)(\?[a-z0-9]+)?$/,
        loader: 'file-loader',
        options: {
          name: './img/[name].[ext]'
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['url-loader']
      },
      {
        test: /\.html$/,
        use: 'html-loader'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Label Image',
      template: path.resolve(SRC_DIR, 'index.ejs'),
      path: path.resolve(DIST_DIR),
      favicon: path.resolve(SRC_DIR, 'assets', 'img', 'favicon.ico'),
      filename: 'index.html',
      inject: true,
      sourceMap: true,
      chunksSortMode: 'dependency',
      minify: {
        collapseWhitespace: devMode
      }
    }),
    new MiniCssExtractPlugin({
      filename: devMode ? '[name].css' : '[name].[hash].css',
      chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
    }),
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      API_URL: JSON.stringify(process.env.API_URL),
      LABEL_APP_BASE_URL: JSON.stringify(process.env.LABEL_APP_BASE_URL)
    }),
    new BaseHrefWebpackPlugin({
      baseHref: process.env.BASE_HREF
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    })
  ],
  stats: {
    children: false,
    entrypoints: false,
    maxModules: 0,
    errors: true
  }
};

module.exports = config;