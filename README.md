

#### Installing all dependencies
```
npm install
```

#### Run the App

Please make sure you've installed all the dependencies using `npm install`. If you run into npm permission issues during installation of npm packages, Follow this link [npm permission issues](https://docs.npmjs.com/getting-started/fixing-npm-permissions) and follow `Option 2`.

```
npm start
```

#### Deploying the app for production

Global variable `NODE_ENV` is set as prefix for every npm command, Here `NODE_ENV` is set to `production` 

```
npm run build
```

After deploying dist folder will be created which comprises of `index.html`, `app.[chunkhash].css`, `vendor.[chunkhash].js`, `app.[chunkhash].js`, `img` directory and any other assets directory if any.

#### Environment Variables
All the environment variables are defined in .env. Change the file `.env.template` to `.env`.
  * `API_URL` - REST API Url
  * `NODE_ENV` - Define then environment (Development/Production).

----
if any queries, please write to [mayuri.jha@crowdanalytix.com](mailto:mayuri.jha@crowdanalytix.com?subject=CGF UI)